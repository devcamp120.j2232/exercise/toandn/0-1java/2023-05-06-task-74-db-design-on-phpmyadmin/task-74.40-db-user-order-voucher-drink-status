-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2023 at 08:39 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devcamp_users_orders_vouchers_drinks_status`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `order_code` varchar(40) NOT NULL,
  `pizza_size` varchar(40) NOT NULL,
  `pizza_type` varchar(40) NOT NULL,
  `voucher_id` int(10) NOT NULL,
  `total_money` int(10) NOT NULL,
  `discount` int(10) NOT NULL,
  `fullname` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `address` varchar(40) NOT NULL,
  `drink_id` int(10) NOT NULL,
  `status_id` int(10) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `pizza_size`, `pizza_type`, `voucher_id`, `total_money`, `discount`, `fullname`, `email`, `address`, `drink_id`, `status_id`, `create_date`, `update_date`) VALUES
(1, 'CK01', 'M', 'Chicken', 1, 250000, 10, 'Nguyễn Văn A', 'a@gmail.com', 'Sài Gòn', 1, 1, 1683441282, 1683441282),
(2, 'HW01', 'M', 'Hawai', 2, 250000, 20, 'Phạm Thị B', 'b@gmail.com', 'Hà Nội', 2, 2, 1683441282, 1683441282),
(3, 'CK02', 'L', 'Chicken', 3, 300000, 30, 'Lê Chí C', 'c@gmail.com', 'Đà Lạt', 3, 3, 1683441532, 1683441532),
(4, 'HW02', 'L', 'Hawai', 4, 300000, 40, 'Phan Chu D', 'd@gmail.com', 'Đà nẵng', 4, 4, 1683441532, 1683441532),
(5, 'HW03', 'S', 'Hawai', 5, 200000, 50, 'Quách Du E', 'e@gmail.com', 'Quảng Trị', 5, 5, 1683441532, 1683441532);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ind_orders_order_code` (`order_code`),
  ADD KEY `voucher_id` (`voucher_id`),
  ADD KEY `drink_id` (`drink_id`),
  ADD KEY `status_id` (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`drink_id`) REFERENCES `drinks` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
